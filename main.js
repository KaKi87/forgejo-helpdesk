import fs from 'node:fs/promises';
import path from 'node:path';

import '@lu.se/console';
import { load as parseYaml } from 'js-yaml';
import { MailListener } from 'mail-listener-type';
import axios from 'axios';
import outdent from 'outdent';
import { configure as configureStringify } from 'safe-stable-stringify';
import Handlebars from 'handlebars';

let
    {
        'imap': {
            'host': imapHost,
            'port': imapPort,
            'is_tls': isImapTls,
            'username': imapUsername,
            'password': imapPassword
        },
        'forgejo': {
            'host': forgejoHost,
            'access_token': forgejoAccessToken,
            'config_repository': forgejoConfigRepository,
            'is_config_remote': isForgejoConfigRemote,
            'repository': {
                'match_type': forgejoRepositoryMatchType,
                'organization': forgejoRepositoryOrganization,
                'topic': forgejoRepositoryTopic
            },
            'repositories': forgejoRepositories,
            'issue': {
                'default_title': forgejoDefaultIssueTitle,
                'reply': forgejoReply
            }
        }
    } = parseYaml(await fs.readFile('./config.yaml')),
    forgejoReplyTemplate = forgejoReply && Handlebars.compile(outdent.string(forgejoReply));

const
    mailListener = new MailListener({
        host: imapHost,
        port: imapPort,
        tls: isImapTls,
        username: imapUsername,
        password: imapPassword,
        fetchUnreadOnStart: true,
        attachments: true,
        attachmentOptions: {
            directory: './data/'
        }
    }),
    forgejoApi = axios.create({
        baseURL: `https://${forgejoHost}/api/v1`,
        params: {
            'access_token': forgejoAccessToken
        }
    }),
    fetchRemoteConfig = async () => {
        ({
            'repository': {
                'match_type': forgejoRepositoryMatchType,
                'organization': forgejoRepositoryOrganization
            },
            'repositories': forgejoRepositories,
            'issue': {
                'default_title': forgejoDefaultIssueTitle,
                'reply': forgejoReply
            }
        } = parseYaml(Buffer.from((await forgejoApi(`/repos/${forgejoConfigRepository.owner}/${forgejoConfigRepository.name}/contents/config.yaml`)).data['content'], 'base64').toString('utf8')));
        forgejoReplyTemplate = forgejoReply && Handlebars.compile(outdent.string(forgejoReply));
    },
    getForgejoUsername = (() => {
        const mailAddressToForgejoUsername = {};
        return async mailAddress => {
            if(!mailAddressToForgejoUsername[mailAddress]) ({
                data: [{
                    'username': mailAddressToForgejoUsername[mailAddress]
                } = {}]
            } = await forgejoApi({
                url: '/admin/emails/search',
                params: {
                    'q': mailAddress
                }
            }));
            return mailAddressToForgejoUsername[mailAddress];
        };
    })(),
    getForgejoRepository = (() => {
        const usernameToRepository = {};
        return async username => {
            if(!usernameToRepository[username]){
                let data;
                if(forgejoRepositoryMatchType === 'collaborator_by_organization') ({
                    data: [data]
                } = await forgejoApi({
                    url: `/orgs/${forgejoRepositoryOrganization}/repos`,
                    params: {
                        'sudo': username
                    }
                }));
                else if(forgejoRepositoryMatchType === 'collaborator_by_topic') ({
                    data: { 'data': [data] }
                } = await forgejoApi({
                    url: '/repos/search',
                    params: {
                        'q': forgejoRepositoryTopic,
                        'topic': true,
                        'sudo': username
                    }
                }));
                usernameToRepository[username] = {
                    owner: data['owner']['username'],
                    name: data['name']
                };
            }
            return usernameToRepository[username];
        };
    })(),
    stringify = configureStringify({ maximumDepth: 3 }),
    objectToHtmlTable = data => outdent `
        <table>
            <thead><tr><th>Key</th><th>Value</th></tr></thead>
            <tbody>${Object.entries(data).map(([key, value]) => `<tr><td>\n\n\`${key}\`</td><td>\n\n\`\`\`${typeof value === 'object' ? `json\n${stringify(value, null, 4)}` : `\n${value}`}\n\`\`\`\n</td></tr>`).join('')}</tbody>
        </table>  
    `,
    reportError = async (error, data) => {
        console.error(error, data);
        await forgejoApi({
            method: 'POST',
            url: `/repos/${forgejoConfigRepository.owner}/${forgejoConfigRepository.name}/issues`,
            data: {
                'title': typeof error === 'string' ? error : error.message,
                'body': outdent `
                    # Forgejo Helpdesk error

                    ${typeof error === 'object' ? outdent `
                        ## Error

                        ${objectToHtmlTable(error)}
                    ` : ''}
                    
                    ${data ? outdent `
                        ## Data

                        ${objectToHtmlTable(data)}
                    ` : ''}
                `
            }
        });
    },
    markMailAsSeen = uid => new Promise((resolve, reject) => mailListener.imap.addFlags(
        uid,
        ['\\Seen'],
        error => {
            if(error)
                reject(error);
            else
                resolve();
        }
    ));

mailListener.on('server:connected', () => console.log('Connected'));

mailListener.on('server:disconnected', () => console.log('Disconnected'));

mailListener.on('error', reportError);

mailListener.on(
    'mail',
    async (data, _, { uid }) => {
        const {
            to: {
                value: [{
                    address: recipientAddress
                }]
            },
            from: {
                value: [{
                    address: senderAddress,
                    name: senderName
                }]
            },
            subject,
            text,
            date,
            attachments
        } = data;
        try {
            if(isForgejoConfigRemote) await fetchRemoteConfig();
            let
                repository,
                username;
            switch(forgejoRepositoryMatchType){
                case null: {
                    repository = forgejoRepositories[0];
                    break;
                }
                case 'recipient_sub_address': {
                    const recipientSubAddress = recipientAddress.split('@')[0].split('+')[1];
                    repository = forgejoRepositories.find(repository => repository.matches.includes(recipientSubAddress.toLowerCase()));
                    break;
                }
                case 'sender_address': {
                    repository = forgejoRepositories.find(repository => repository.matches.includes(senderAddress.toLowerCase()));
                    break;
                }
                case 'collaborator_by_organization':
                case 'collaborator_by_topic': {
                    username = await getForgejoUsername(senderAddress);
                    repository = await getForgejoRepository(username);
                    break;
                }
            }
            if(!repository) return reportError('No matching repository', data);
            if(!username) username = await getForgejoUsername(senderAddress);
            if(!username) return reportError('No matching username', data);
            const {
                data: {
                    'number': issueNumber
                }
            } = await forgejoApi({
                method: 'POST',
                url: `/repos/${repository.owner}/${repository.name}/issues`,
                params: {
                    'sudo': username
                },
                data: {
                    'title': subject || forgejoDefaultIssueTitle,
                    'body': outdent `
                        ${text}

                        ---
                        Date : \`${date.toISOString()}\`
                    `
                }
            });
            if(attachments.length){
                for(const attachment of attachments){
                    const formData = new FormData();
                    formData.append('attachment', new Blob([attachment.content]), attachment.filename);
                    await forgejoApi({
                        method: 'POST',
                        url: `/repos/${repository.owner}/${repository.name}/issues/${issueNumber}/assets`,
                        params: {
                            'name': attachment.filename
                        },
                        data: formData
                    });
                    await fs.unlink(path.join('./data', attachment.filename));
                }
            }
            if(forgejoReplyTemplate) await forgejoApi({
                method: 'POST',
                url: `/repos/${repository.owner}/${repository.name}/issues/${issueNumber}/comments`,
                data: {
                    'body': forgejoReplyTemplate({
                        name: senderName || username,
                        issueNumber
                    })
                }
            });
            await markMailAsSeen(uid);
        }
        catch(error){
            await reportError(error, data);
        }
    }
);

mailListener.start();