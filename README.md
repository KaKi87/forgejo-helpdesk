# Forgejo Helpdesk

Turn messy e-mails into organized [Forgejo](https://codeberg.org/forgejo/forgejo) issues in a helpdesk-like way (e.g. ZenDesk ticketing).